import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class AccordionIndexComponent extends Component {

    @action
    activate(element) {
        jQuery(element).accordion();
    }
}
