import Component from '@glimmer/component';
import {action} from '@ember/object';

export default class SearchIndexComponent extends Component {

    get showClearTextIcon() {
        return this.args.showClearTextIcon && this.args.selected && this.args.selected.length > 0;
    }

    @action
    searchChanged(element) {
        if(this.args.onChange) {
            this.args.onChange(element.target.value || '');
        }
    }

    @action
    clearSearchText() {
        if(this.args.onChange) {
            this.args.onChange('');
        }
    }
}
