import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ModalRequestReportComponent extends Component {

    @tracked recipient;

    @action
    activateModal() {
        this.recipient = this.args.recipient;
    }

    @action
    setRecipient(event) {
        this.recipient = event.target.value;
    }

    @action
    requestCsvReportAction() {
        this.args.requestCsvReportAction(this.recipient);
    }
}
