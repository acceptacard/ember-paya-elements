import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class CheckboxRadioComponent extends Component {
    @action
    setCheckedOrUnchecked(element) {
        jQuery(element).checkbox();

        if (this.args.checked) {
            jQuery(element).checkbox('check');
        } else {
            jQuery(element).checkbox('uncheck');
        }
    }

    @action
    clicked() {
        if (this.args.onChange) {
            this.args.onChange(!this.args.checked);
        }
    }
}
