import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class PaginationIndexComponent extends Component {

    defaultLinks = {
        'first': {
            icon: 'step backward icon'
        },
        'prev': {
            icon: 'horizontally flipped play icon'
        },
        'next': {
            icon: 'play icon'
        },
        'last': {
            icon: 'step forward icon'
        }
    };

    @action
    updatePagination(link) {
        let decodedLink = decodeURIComponent(link);
        let queryParams = {};
        let splitLink = decodedLink.split('?');

        if (splitLink.length > 1) {
            splitLink.slice(1)[0].split('&').forEach(pairs => {
                let index = pairs.split('=')[0];
                let value = pairs.split('=')[1];

                // If a nested query param (E.g. page[offset]).
                let firstBracket = index.indexOf('[');

                if (firstBracket > -1) {
                    let lastBracket = index.indexOf(']');
                    let key = index.substring(firstBracket + 1, lastBracket);
                    queryParams[key] = value;
                } else {
                    queryParams[index] = value;
                }
            });

        } else {
            queryParams['offset'] = '0';
        }

        // Call action on parent controller.
        this.args.updateQueryParams(queryParams);
    }
}
