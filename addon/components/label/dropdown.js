import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

const VALID_SETTINGS = [];

export default class LabelDropdownComponent extends Component {

    @action
    activateDropdown(element) {
        let settings = {};

        VALID_SETTINGS.forEach(setting => {
            if(this.args[setting]) {
                settings[setting] = this.args[setting];
            }
        });

        jQuery(element).dropdown(settings);

        if(this.args.selected != null && this.args.selected !== '') {
            jQuery(element).dropdown('set selected', this.args.selected);
        } else {
            jQuery(element).dropdown('clear');
        }
    }

    @action
    updateDropdown(element) {
        if(this.args.onChange) {
            this.args.onChange(element.target.value);
        }
    }
}
