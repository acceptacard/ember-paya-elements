import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class NagIndexComponent extends Component {

    @action
    activate(element) {
        jQuery(element).nag('show');
    }
}
