import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

const VALID_SETTINGS = ['fullTextSearch'];

export default class DropdownSelectionComponent extends Component {

    @action
    activateDropdown(element) {
        let settings = {};

        VALID_SETTINGS.forEach(setting => {
            if(this.args[setting]) {
                settings[setting] = this.args[setting];
            }
        });

        jQuery(element).dropdown(settings);
    }

    @action
    hasChanged(element) {
        if(this.args.onChange) {
            this.args.onChange(element.target.value);
        }
    }
}
