import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class MenuDropdownItemComponent extends Component {

    @action
    activateDropdown(element) {
        jQuery(element).dropdown();
    }
}
