import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class MenuDropdownComponent extends Component {

    @action
    activateDropdown(element) {
        jQuery(element).find('.dropdown.item').dropdown();
    }
}
