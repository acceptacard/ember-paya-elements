import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class SidebarIndexComponent extends Component {

    @action
    activate(element) {
        const config = {};
        config.dimPage = this.args.dimPage;

        jQuery(element).sidebar(config);
    }
}
