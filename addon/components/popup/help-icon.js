import Component from '@glimmer/component';
import { action } from '@ember/object';
import jQuery from 'jquery';

export default class PopupHelpIconComponent extends Component {

    @action
    activate(element) {
        jQuery(element).popup({
            on: 'hover'
        });
    }
}
