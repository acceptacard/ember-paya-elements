import Component from '@glimmer/component';

export default class FormFieldComponent extends Component {

    get hasErrors() {
        return this.args.errors && this.args.errors.length > 0;
    }

    get errors() {
        return this.args.errors && Array.isArray(this.args.errors) ? this.args.errors : this.args.errors ? [this.args.errors] : null;
    }
}
