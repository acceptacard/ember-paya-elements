import Component from '@glimmer/component';
import {inject as service} from '@ember/service';

export default class CharityQrCodeComponent extends Component {

    @service numeral;

    get customAmounts() {
        return this.args.queryParams.custom_amounts ? this.args.queryParams.custom_amounts.split(',')
            .map(customAmount => this.numeral.getFormattedCurrencyFromPoundsAmount(customAmount)).join(', ') : '';
    }

    get noCacheImage() {
        return `${this.args.imageUrl}?nocache=${Date.now()}`;
    }
}
