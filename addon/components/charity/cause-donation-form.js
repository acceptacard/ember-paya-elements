import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { task, timeout } from 'ember-concurrency';
import {ISO_CURRENCY_CODE_GBP} from 'ember-paya-elements/services/numeral';

const DEFAULT_ID = '1';
const TYPE_EKASHU_CREDENTIALS = 'EKASHU_CREDENTIALS';
const TYPE_CHARITIES_TRUST_BENEFICIARIES = 'CHARITIES_TRUST_BENEFICIARIES';

const EKASHU_CREDENTIALS_TYPE_CHARITIES_TRUST = 1;
const PRESET_AMOUNTS = ['5.00', '10.00', '20.00'];
export const VALID_AMOUNT_MINIMUM = 50;
export const VALID_AMOUNT_MAXIMUM = 999999;

export const PERIOD_SINGLE = 'single';
export const PERIOD_WEEKLY = 'weekly';
export const PERIOD_MONTHLY = 'monthly';

const PERIODS = {
    [PERIOD_SINGLE]: 'SINGLE',
    [PERIOD_WEEKLY]: 'WEEKLY',
    [PERIOD_MONTHLY]: 'MONTHLY'
};


export default class CharityCauseDonationFormComponent extends Component {

    @service errorHandler;
    @service userData;
    @service store;
    @service notifications;
    @service numeral;
    @service localStorage;

    @tracked filteredCauses;
    @tracked filteredCharities;
    @tracked showCustomAmountsModal = false;
    @tracked showPreview = this.localStorage.getItem('showDonationPagePreview') !== false;

    constructor() {
        super(...arguments);

        if(!this.args.editing) {
            this.args.form.hideGiftAid = !this.showGiftAidToggle;
        }
    }

    get showGiftAidToggle() {
        return this.userData.merchant ? this.userData.currencyCode === ISO_CURRENCY_CODE_GBP : true;
    }

    get presetAmounts() {
        return this.args.form.customAmounts ? this.args.form.customAmounts : (this.args.presetAmounts ? this.args.presetAmounts : PRESET_AMOUNTS);
    }

    get defaultAmountsForSelectedCharity() {
        return this.args.form.presetAmounts || PRESET_AMOUNTS;
    }

    get periods() {
        return this.args.form.periods ? this.args.form.periods : PERIODS;
    }

    get registeredCharityNumber() {
        return this.userData.merchant ? this.userData.merchant.traderCompanyNumber : (this.args.form.selectedCharity ? this.args.form.selectedCharity.registeredCharityNumber : null);
    }

    get showCauses() {
        return !this.args.hideCauseSelection && (this.args.causesLoading || (this.args.ekashuCredentials && this.args.ekashuCredentials.causes));
    }

    get hidePeriods() {
        return this.args.form.hidePeriods || (this.args.ekashuCredentials && this.args.ekashuCredentials.hideRegistration);
    }

    get showGeolocationCheckbox() {
        return !this.args.form.selectedCause && this.args.ekashuCredentials && this.args.ekashuCredentials.causes
            ? this.args.ekashuCredentials.causes.map(cause => cause).some(cause => cause.geolocation) : false;
    }

    resetCustomAmounts() {
        if(this.args.donorCustomAmounts) {
            this.args.form.customAmount1 = this.args.donorCustomAmounts.amount_1;
            this.args.form.customAmount2 = this.args.donorCustomAmounts.amount_2;
            this.args.form.customAmount3 = this.args.donorCustomAmounts.amount_3;
        } else {
            this.args.form.customAmount1 = this.defaultAmountsForSelectedCharity[0];
            this.args.form.customAmount2 = this.defaultAmountsForSelectedCharity[1];
            this.args.form.customAmount3 = this.defaultAmountsForSelectedCharity[2];
        }
    }

    @task({
        restartable: true
    })
    *taskSearchForCharity(searchString) {
        try {
            yield timeout(250);

            if(searchString.length === 0) {
                this.args.form.selectedCharity = null;
                this.filteredCharities = [];

                if(!this.args.editing && this.args.startAgain) {
                    this.args.startAgain();
                }
            } else if(searchString.length > 3) {
                this.filteredCharities = yield this.store.query('charity-search', {
                    q: searchString
                });
            }
        } catch(e) {
            this.errorHandler.handleErrors(e);
        }
    }

    @task({
        restartable: true
    })
    *taskSearchForCause(searchString) {
        try {
            yield timeout(250);

            if(searchString.length === 0) {
                this.args.form.selectedCause = null;
                this.filteredCauses = [];

                if(!this.args.editing && this.args.startAgain) {
                    this.args.startAgain();
                }
            } else {
                this.filteredCauses = this.args.ekashuCredentials.causes.filter(cause => cause.name.toLowerCase().includes(searchString.toLowerCase()));
            }
        } catch(e) {
            this.errorHandler.handleErrors(e);
        }
    }

    @task({
        drop: true
    })
    *taskSaveCustomAmounts() {
        try {
            let successMessage = 'Custom amounts saved successfully. These new amounts will be used for all future donations unless changed again.';

            // Make sure this is the MP and set these amounts as default for this account.
            if(this.userData.merchant && this.args.form.makeCustomAmountsDefault) {
                if(this.args.donorCustomAmounts) {
                    // No defaults currently exist for this merchant as they are using our standard default.
                    if(this.args.donorCustomAmounts.id === DEFAULT_ID) {
                        let type;
                        let typeId;

                        if(this.args.ekashuCredentials) {
                            type = TYPE_EKASHU_CREDENTIALS;
                            typeId = this.args.ekashuCredentials.id;
                        } else if(this.userData.merchant.charitiesTrustId) {
                            type = TYPE_CHARITIES_TRUST_BENEFICIARIES;
                            typeId = this.userData.merchant.charitiesTrustId;
                        }

                        if(type && typeId) {
                            let donorCustomAmount = this.store.createRecord('donor-custom-amount', {
                                typeId: typeId,
                                type: type,
                                amount_1: this.args.form.customAmount1,
                                amount_2: this.args.form.customAmount2,
                                amount_3: this.args.form.customAmount3
                            });

                            this.userData.donorCustomAmounts = yield donorCustomAmount.save();
                            this.notifications.success(successMessage);
                        } else {
                            this.notifications.error('The default donation amounts you have selected could not be saved because your account type could not be established.');
                        }
                    } else {
                        this.args.donorCustomAmounts.amount_1 = this.args.form.customAmount1;
                        this.args.donorCustomAmounts.amount_2 = this.args.form.customAmount2;
                        this.args.donorCustomAmounts.amount_3 = this.args.form.customAmount3;
                        yield this.args.donorCustomAmounts.save();

                        this.notifications.success(successMessage);
                    }
                }
            } else {
                this.notifications.success('Custom amounts saved successfully.');
            }

            this.toggleCustomAmountsModal();
        } catch(e) {
            this.errorHandler.handleErrors(e);
        }
    }

    @action
    updatePresetAmount(presetAmount) {
        this.args.form.customAmount = null;
        this.args.form.presetAmount = presetAmount;
    }

    @action
    updateCustomAmount(customAmount) {
        this.args.form.presetAmount = null;
        this.args.form.customAmount = customAmount;
    }

    @action
    updatePeriod(period) {
        this.args.form.period = period;
    }

    @action
    toggleHidePeriods() {
        this.args.form.hidePeriods = !this.args.form.hidePeriods;

        if(!this.args.form.hidePeriods) {
            if(this.args.form.hideRegistration) {
                this.notifications.info('If you show donation frequency types then the registration option must also be displayed because weekly and monthly recurring donations require registration.');
            }

            this.args.form.hideRegistration = false;
        }
    }

    @action
    toggleHidePresetAmounts() {
        this.args.form.hidePresetAmounts = !this.args.form.hidePresetAmounts;

        if(!this.args.form.hidePresetAmounts && this.args.form.fixedAmount) {
            this.args.form.fixedAmount = false;
            this.notifications.info('If you show preset amounts then you cannot set a fixed donation amount that the donor cannot change.');

        }
    }

    @action
    toggleHideGiftAid() {
        this.args.form.hideGiftAid = !this.args.form.hideGiftAid;
    }

    @action
    toggleHideRegistration() {
        this.args.form.hideRegistration = !this.args.form.hideRegistration;

        if(this.args.form.hideRegistration && !this.args.form.hidePeriods) {
            this.args.form.hidePeriods = true;
            this.notifications.info('If you hide the registration option then donation frequency types must also be hidden because weekly and monthly recurring donations require registration.');
        }
    }

    @action
    toggleFixedDonation() {
        this.args.form.fixedAmount = !this.args.form.fixedAmount;

        if(this.args.form.fixedAmount && !this.args.form.hidePresetAmounts) {
            this.args.form.hidePresetAmounts = true;
            this.notifications.info('If you show a fixed donation amount that the donor cannot change then preset amounts will not be displayed.');
        }

        this.args.form.presetAmount = null;
    }

    @action
    updateReference(event) {
        this.args.form.reference = event.target.value;
        this.args.form.selectedCause = null;
    }

    @action
    searchForCause(searchString) {
        this.args.form.reference = null;
        this.taskSearchForCause.perform(searchString);
    }

    @action
    searchForCharity(searchString) {
        this.args.form.reference = null;
        this.taskSearchForCharity.perform(searchString);
    }

    @action
    causeSelected(cause) {
        this.args.form.selectedCause = cause;
        this.args.form.reference = cause.reference;
        this.args.form.geolocation = false;
        this.filteredCauses = [];
    }

    @action
    charitySelected(charity) {
        this.args.form.selectedCause = null;
        this.args.form.selectedCharity = charity;
        this.args.form.reference = charity.type == EKASHU_CREDENTIALS_TYPE_CHARITIES_TRUST ? charity.reference : null;
        this.filteredCharities = [];

        if(this.args.form.selectedCharity
            && this.args.form.selectedCharity.type != EKASHU_CREDENTIALS_TYPE_CHARITIES_TRUST
            && this.args.reloadEkashuCredentials) {
            this.args.reloadEkashuCredentials();
        }
    }

    @action
    toggleCustomAmountsModal(event) {
        if(event) {
            event.preventDefault();
        }

        this.showCustomAmountsModal = !this.showCustomAmountsModal;

        // If opening the modal then clear the checkbox.
        if(this.showCustomAmountsModal) {
            this.args.form.makeCustomAmountsDefault = false;

            this.resetCustomAmounts();
        }
    }

    @action
    cancelCustomAmountsModal() {
        this.resetCustomAmounts();
        this.toggleCustomAmountsModal();
    }

    @action
    updateAmount(attribute, value) {
        this.args.form[attribute] = value;
    }

    @action
    toggleMakeCustomAmountsDefault(value) {
        this.args.form.makeCustomAmountsDefault = value;
    }

    @action
    updateCustomAmounts() {
        if([this.args.form.customAmount1, this.args.form.customAmount2, this.args.form.customAmount3]
            .filter(amount => {
                let integerAmount = this.numeral.getIntegerFromStringCurrency(amount);
                return integerAmount >= VALID_AMOUNT_MINIMUM && integerAmount <= VALID_AMOUNT_MAXIMUM;
            })
            .reduce((uniqueArray, amount) => uniqueArray.includes(amount) ? uniqueArray : [...uniqueArray, amount], []).length === 3
        ) {
            this.args.form.presetAmount = null;
            this.taskSaveCustomAmounts.perform();
        } else {
            this.notifications.error('Please ensure all amounts are different and in the range £0.50 to £9,999.99.');
        }
    }

    @action
    togglePreview() {
        this.showPreview = !this.showPreview;
        this.localStorage.setItem('showDonationPagePreview', this.showPreview);
    }

    @action
    toggleGeolocation() {
        this.args.form.geolocation = !this.args.form.geolocation;
    }

    @action
    toggleAllowEmail() {
        this.args.form.allowEmail = !this.args.form.allowEmail;
    }
}
