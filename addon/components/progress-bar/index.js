import Component from '@glimmer/component';
import {action} from '@ember/object';
import jQuery from 'jquery';

export default class ProgressBarIndexComponent extends Component {

    @action
    activate(element) {
        jQuery(element).progress({
            percent: 0
        });
    }

    @action
    update(element) {
        jQuery(element).progress('set percent', parseInt(this.args.percent));
    }
}
