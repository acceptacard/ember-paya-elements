import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class DatagridSearchBarComponent extends Component {

    dropdownLimitOptions = [
        {text: '5', value: '5'},
        {text: '10', value: '10'},
        {text: '25', value: '25'},
        {text: '50', value: '50'},
        {text: '100', value: '100'},
    ];

    @action
    updateLimit(limit) {
        this.args.updateQueryParams({
            limit: limit
        });
    }

    @action
    updateSearchWithinDateRange(searchWithinDateRange) {
        this.args.updateQueryParams({
            searchWithinDateRange: searchWithinDateRange
        });
    }
}
