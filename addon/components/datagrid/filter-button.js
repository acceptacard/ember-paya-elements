import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class DatagridFilterButtonComponent extends Component {

    @action
    filterResults() {
        this.args.filterResults();
    }
}
