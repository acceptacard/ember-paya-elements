import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class DatagridIndexComponent extends Component {

    @service media;
    @service router;

    @tracked sortDirection = this.queryParams.sort.startsWith('-') ? '-' : '';
    @tracked sortColumn = this.queryParams.sort.replace(/^-/, '');

    get queryParams() {
        return Object.assign({}, this.args.defaultQueryParamValues, this.router.currentRoute.queryParams);
    }

    get meta() {
        return this.args.data && this.args.data.meta ? this.args.data.meta : null;
    }

    get responsive() {
        return this.media.isTablet || this.media.isMobile;
    }

    get rowInfoText() {
        let total = 0;
        let intOffset = 0;
        let intLimit = 0;

        if (this.meta && this.meta.total) {
            total = parseInt(this.meta.total);
        }

        if (this.queryParams.limit === 'all') {
            return `Showing all ${total} rows`;
        } else {
            if (total === 0) {
                intOffset = parseInt(this.queryParams.offset);
            } else {
                intOffset = parseInt(this.queryParams.offset) + 1;
            }

            intLimit = parseInt(this.queryParams.limit) - 1;

            let lastRowShown = intOffset + intLimit;

            if (total < lastRowShown) {
                lastRowShown = total;
            }

            return `${intOffset} to ${lastRowShown} of ${total}`;
        }
    }

    get showNoDataMessage() {
        return this.args.data && this.args.data.content && this.args.data.content.length === 0 && !this.args.loading;
    }

    @action
    updateQueryParams(queryParams) {
        this.args.updateQueryParams(queryParams);
    }

    @action
    filterBySearchText(searchText) {
        this.args.filterByText(searchText);
    }

    @action
    sortByColumn(column) {
        this.sortColumn = column;
        this.sortDirection = this.sortDirection === '' ? '-' : '';
        this.updateQueryParams({
            sort: `${this.sortDirection}${column}`
        });
    }

    @action
    rowClicked(row) {
        if(this.args.clickable) {
            this.args.rowClicked(row);
        }
    }

    @action
    backToTop() {
        window.scrollTo(0, 0);
    }
}
