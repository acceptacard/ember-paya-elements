import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class DatagridControlBarComponent extends Component {

    @service media;
    @service router;

    @tracked showFilters = this.args.showFilters || false;

    get queryParams() {
        return Object.assign({}, this.args.defaultQueryParamValues, this.router.currentRoute.queryParams);
    }

    @action
    toggleShowFilters() {
        this.showFilters = !this.showFilters;
    }

    @action
    filterCheckbox() {
        this.args.filterCheckbox();
    }

    @action
    changeFromDate(dates, dateString) {
        this.args.changeFromDate(dates, dateString);
    }

    @action
    changeToDate(dates, dateString) {
        this.args.changeToDate(dates, dateString);
    }

    @action
    refreshDatagrid() {
        if(this.media.isMobile) {
            this.performingRefresh = true;
        }

        this.args.refreshDatagrid();
    }

    @action
    resetFilters() {
        this.args.resetFilters();
    }

    @action
    requestCsvReport() {
        if(this.args.requestReport) {
            this.args.requestCsvReport();
        }
    }
}
