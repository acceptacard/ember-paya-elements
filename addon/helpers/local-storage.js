import Helper from '@ember/component/helper';
import { inject as service } from '@ember/service';
import { LOCAL_STORAGE_NAMESPACE_USER_SETTINGS } from '../services/local-storage';

export default class HelpersLocalStorage extends Helper {
    @service localStorage;

    compute([key, namespace= LOCAL_STORAGE_NAMESPACE_USER_SETTINGS]) {
        return this.localStorage.getItem(key, namespace);
    }
}
