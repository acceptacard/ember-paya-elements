import Helper from "@ember/component/helper";
import { inject as service } from '@ember/service';

export default class FormatPenceAsCurrency extends Helper {

    @service numeral;

    compute([pence, isoCurrencyCode]) {
        return this.numeral.getFormattedCurrencyFromPenceAmount(pence, isoCurrencyCode);
    }
}
