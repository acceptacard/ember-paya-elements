import Helper from "@ember/component/helper";
import { inject as service } from '@ember/service';

export default class FormatDecimalAsCurrencyHelper extends Helper {

    @service numeral;

    compute([decimal]) {
        return this.numeral.getIntegerFromStringCurrency(decimal);
    }
}
