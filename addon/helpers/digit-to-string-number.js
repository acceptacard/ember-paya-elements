import { helper } from '@ember/component/helper';

const MAP_DIGIT_TO_STRING_NUMBER = {
    0: 'zero',
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six',
    7: 'seven',
    8: 'eight',
    9: 'nine',
    10: 'ten',
    11: 'eleven',
    12: 'twelve',
    13: 'thirteen',
    14: 'fourteen',
    15: 'fifteen'
};

export default helper(function digitToStringNumber(params) {
    if(Object.keys(MAP_DIGIT_TO_STRING_NUMBER).includes(params[0].toString())) {
        return MAP_DIGIT_TO_STRING_NUMBER[params[0]];
    }
});
