import { helper } from '@ember/component/helper';

export default helper(function div(numbers) {
    return numbers.reduce((a, b) => Number(a) / Number(b));
});
