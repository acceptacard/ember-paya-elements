import { helper } from '@ember/component/helper';

export default helper(function ceil([number]) {
    return Math.ceil(number);
});
