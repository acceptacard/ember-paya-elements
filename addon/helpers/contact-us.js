import Helper from '@ember/component/helper';
import {inject as service} from '@ember/service';
import { htmlSafe } from '@ember/template';

export default class ContactUs extends Helper {
    @service intl;

    compute() {
        return htmlSafe(`If you have any questions or need help with our service please call us on
            <a href="tel:${this.intl.t('partner.customer_services_phone')}">
            ${this.intl.t('partner.customer_services_phone')}</a>.  Lines are open Monday to Friday 9am-5pm.
            Outside of these hours, please email
            <a href="mailto:${this.intl.t('partner.customer_services_email')}">
            ${this.intl.t('partner.customer_services_email')}</a> and a member of the support team will respond
            as soon as possible.`);
    }
}
