import Helper from "@ember/component/helper";
import { inject as service } from '@ember/service';

export default class FormatDecimalAsCurrency extends Helper {

    @service numeral;

    compute([pounds, isoCurrencyCode]) {
        return this.numeral.getFormattedCurrencyFromPoundsAmount(pounds, isoCurrencyCode);
    }
}
