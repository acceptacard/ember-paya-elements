import Helper from "@ember/component/helper";
import { inject as service } from '@ember/service';

export default class GetNumeral extends Helper {

    @service numeral;

    compute([value, format = null]) {
        return this.numeral.getNumeral(value, format);
    }
}
