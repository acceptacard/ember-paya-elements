import { helper } from '@ember/component/helper';

export default helper(function generateDocumentDownloadLink([url, id, jwt]) {
    return `${url}/${id}?token=${jwt}`;
});
