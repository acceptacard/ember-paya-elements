import { helper } from '@ember/component/helper';
import { DEFAULT_DATE_TIME_FORMAT, MYSQL_DATE_TIME_FORMAT } from '../modules/datetime';
import moment from 'moment';

export default helper(function mysqlToUkDateTime(params) {
    if(params[0]) {
        return moment(params[0], MYSQL_DATE_TIME_FORMAT).format(DEFAULT_DATE_TIME_FORMAT);
    }
});
