export const DEFAULT_DATAGRID_OFFSET = '0';
export const DEFAULT_DATAGRID_LIMIT = '10';
export const DEFAULT_DATAGRID_SORT_COLUMN = '-id';

export const DEFAULT_DATAGRID_QUERY_PARAMS = [
    'offset', 'limit', 'searchAttributes', 'searchText', 'sort', 'searchWithinDateRange', 'fromDate', 'toDate',
    'customDate'
];

export const DEFAULT_DATAGRID_ROUTE_QUERY_PARAMS = {
    offset: {
        refreshModel: true
    },
    limit: {
        refreshModel: true
    },
    searchAttributes: {
        refreshModel: true
    },
    searchText: {
        refreshModel: true
    },
    sort: {
        refreshModel: true
    },
    fromDate: {
        refreshModel: false
    },
    toDate: {
        refreshModel: false
    },
    customDate: {
        refreshModel: true
    },
    searchWithinDateRange: {
        refreshModel: true
    }
};

export function generateDatagridApiRequestQueryParameters(params) {
    return {
        filter: {
            from: params.fromDate,
            to: params.toDate,
            search_attributes: params.searchAttributes,
            search_text: params.searchText,
            search_within_date_range: params.searchWithinDateRange
        },
        page: {
            offset: params.offset,
            limit: params.limit
        },
        sort: params.sort
    };
}
