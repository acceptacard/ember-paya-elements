import { DEFAULT_DATAGRID_LIMIT, DEFAULT_DATAGRID_OFFSET, DEFAULT_DATAGRID_SORT_COLUMN } from "./constants";

export default class DatagridQueryParams {
    offset = DEFAULT_DATAGRID_OFFSET;
    limit = DEFAULT_DATAGRID_LIMIT;
    sort = DEFAULT_DATAGRID_SORT_COLUMN;
    searchAttributes = null;
    searchText = null;
    fromDate = null;
    toDate = null;
    customDate = null;
}
