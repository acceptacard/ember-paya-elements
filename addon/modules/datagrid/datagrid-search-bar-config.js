import { tracked } from '@glimmer/tracking';

export default class DatagridSearchBarConfig {

    @tracked showDisplayedRows = true;
    @tracked showTextFilter = true;
    @tracked showPagination = true;
    @tracked showSearchCheckbox = false;
    @tracked height = '500px';
    @tracked searchPlaceholderText = 'Search...';
}
