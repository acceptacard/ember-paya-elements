const YES = 'Yes';
const NO = 'No';

export const toYesNo = value => {
    return ['y', "1", 1, true].includes(value) ? YES : NO;
};

export const getNested = (nestedObj, pathArr) => pathArr.reduce((obj, key) => (obj && obj[key] !== 'undefined') ? obj[key] : undefined, nestedObj);

export const urlToQueryParamsObject = (requestUrl) => Array.from((new URLSearchParams(requestUrl.split('?')[1])).entries()).reduce((obj, qp) => ({ ...obj, [qp[0]]: qp[1]}), {});
