export const REGEX_NAME_ON_CARD = /^[a-zA-Z0-9 '.-]*$/;
export const REGEX_DECIMAL_CURRENCY = /^(?![.0]*$)\d+\.\d{2}$/;