import Service from '@ember/service';
import { inject as service } from '@ember/service';
import numeral from 'numeral';
import 'numeral/locales/en-gb';
import {REGEX_DECIMAL_CURRENCY} from '../modules/regex';

export const ISO_CURRENCY_CODE_GBP = 'GBP';
export const ISO_CURRENCY_CODE_EURO = 'EUR';

// Republic of Ireland (EURO)
if (numeral.locales['en-ie'] === undefined) {
    numeral.register('locale', 'en-ie', {
        delimiters: {
            thousands: ',',
            decimal: '.'
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal: function (number) {
            var b = number % 10;
            return (~~(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                    (b === 2) ? 'nd' :
                        (b === 3) ? 'rd' : 'th';
        },
        currency: {
            symbol: '€'
        }
    });
}

const DEFAULT_CURRENCY_FORMAT = '$0,0.00';
const DEFAULT_CURRENCY_NO_PENCE_FORMAT = '$0,0[.]00';

const LOCALE_EN_GB = 'en-gb';
const LOCALE_EN_IE = 'en-ie';

const VALID_ISO_CURRENCY_CODES = [
    ISO_CURRENCY_CODE_GBP,
    ISO_CURRENCY_CODE_EURO
];

const ISO_CURRENCY_CODE_TO_LOCALE = {
    [ISO_CURRENCY_CODE_GBP]: LOCALE_EN_GB,
    [ISO_CURRENCY_CODE_EURO]: LOCALE_EN_IE
};

export default class NumeralService extends Service {

    @service userData;

    get locale() {
        return VALID_ISO_CURRENCY_CODES.includes(this.userData.isoCurrencyCode)
            ? ISO_CURRENCY_CODE_TO_LOCALE[this.userData.isoCurrencyCode] : LOCALE_EN_GB;
    }

    getNumeral(value, format = null, isoCurrencyCode = null) {
        // An ISO currency code has been supplied to overwrite this user's default locale.
        if(isoCurrencyCode && VALID_ISO_CURRENCY_CODES.includes(isoCurrencyCode)) {
            numeral.locale(ISO_CURRENCY_CODE_TO_LOCALE[isoCurrencyCode])
        } else {
            numeral.locale(this.locale);
        }

        if (format != null) {
            return numeral(value).format(format);
        }

        return numeral(value).value();
    }

    getFormattedCurrencyFromPenceAmount(value, isoCurrencyCode = null) {
        if(value != null) {
            if(value == 0) {
                return this.getNumeral(numeral(value).value(), DEFAULT_CURRENCY_FORMAT, isoCurrencyCode);
            }

            return this.getNumeral(numeral(value).divide(100).value(), DEFAULT_CURRENCY_FORMAT, isoCurrencyCode);
        }

        return '';
    }

    getIntegerFromStringCurrency(value) {
        if(value != null && value !== '') {
            numeral.locale(this.locale);
            return numeral(value).multiply(100).value();
        }

        return 0;
    }

    getFormattedCurrencyFromPoundsAmount(value, isoCurrencyCode = null) {
        if(value != null) {
            return this.getNumeral(numeral(value).value(), DEFAULT_CURRENCY_FORMAT, isoCurrencyCode);
        }

        return '';
    }

    getRoundedFormattedCurrencyFromPenceAmount(value, isoCurrencyCode = null) {
        if(value != null) {
            if(value == 0) {
                return this.getNumeral(numeral(value).value(), DEFAULT_CURRENCY_NO_PENCE_FORMAT, isoCurrencyCode);
            }

            return this.getNumeral(Math.round(numeral(value).divide(100).value()), DEFAULT_CURRENCY_NO_PENCE_FORMAT, isoCurrencyCode);
        }

        return '';
    }

    getRoundedFormattedCurrencyFromPoundsAmount(value) {
        if(value != null) {
            return this.getNumeral(Math.round(numeral(value).value()), DEFAULT_CURRENCY_NO_PENCE_FORMAT);
        }

        return '';
    }

    validateDecimalCurrencyAmount(amount, allowZero = false) {
        if(REGEX_DECIMAL_CURRENCY.test(amount)) {
            if(!allowZero) {
                return this.getIntegerFromStringCurrency(amount) > 0;
            }

            return true;
        }

        return false;
    }
}
