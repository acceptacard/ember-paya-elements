import Service, { inject as service } from '@ember/service';

export default class RouteAuthService extends Service {

    @service session;
    @service router;
    @service can;

    checkAccess(requiredPermissions = null) {
        if(this.session.isAuthenticated) {
            if(requiredPermissions) {
                if(Array.isArray(requiredPermissions)) {
                    if(requiredPermissions.some(requiredPermission => this.can.can(requiredPermission))) {
                        return true;
                    }
                } else if(this.can.can(requiredPermissions)) {
                    return true;
                }

                this.router.transitionTo('access-denied');
                return false;
            }

            return true;
        }

        this.router.transitionTo('login');
    }
}
