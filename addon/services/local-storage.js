import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { A } from '@ember/array';

const LOCAL_STORAGE_NAMESPACE_PREFIX = 'storage:';
export const LOCAL_STORAGE_NAMESPACE_USER_SETTINGS = 'user-settings';
export const LOCAL_STORAGE_NAMESPACE_BASKET = 'basket';
const VALID_NAMESPACES = [
    LOCAL_STORAGE_NAMESPACE_USER_SETTINGS, LOCAL_STORAGE_NAMESPACE_BASKET
];

export default class LocalStorageService extends Service {

    @tracked storage = window.localStorage;

    getDataByNamespace(namespace, isArray = false) {
        let data;

        if (VALID_NAMESPACES.includes(namespace)) {
            data = this.storage.getItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace);
        }

        return isArray ? A(JSON.parse(data)) : JSON.parse(data);
    }


    getItem(key, namespace = LOCAL_STORAGE_NAMESPACE_USER_SETTINGS) {
        let data = this.getDataByNamespace(namespace);

        if(data && data.hasOwnProperty(key)) {
            return data[key];
        }

        return undefined;
    }

    setItem(key, value, namespace = LOCAL_STORAGE_NAMESPACE_USER_SETTINGS) {
        if(this.storage.getItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace) === null) {
            this.clearItems(namespace);
        }

        let data = this.getDataByNamespace(namespace);
        data[key] = value;

        if(data) {
            this.storage.setItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace, JSON.stringify(data));
            this.storage = window.localStorage;
        }
    }

    setArrayData(data, namespace = LOCAL_STORAGE_NAMESPACE_USER_SETTINGS) {
        if(this.storage.getItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace) === null) {
            this.clearItems(namespace, true);
        }

        this.storage.setItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace, JSON.stringify(data));
        this.storage = window.localStorage;
    }

    removeItem(key, namespace = LOCAL_STORAGE_NAMESPACE_USER_SETTINGS, isArray = false) {
        let data = this.getDataByNamespace(namespace, isArray);

        if(data && ((isArray && data[key]) || (!isArray && data.hasOwnProperty(key)))) {
            delete data[key];
            this.storage.setItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace, JSON.stringify(data));
            this.storage = window.localStorage;
        }
    }

    clearItems(namespace = LOCAL_STORAGE_NAMESPACE_USER_SETTINGS, isArray = false) {
        this.storage.setItem(LOCAL_STORAGE_NAMESPACE_PREFIX + namespace, isArray ? '[]' : '{}');
    }
}
