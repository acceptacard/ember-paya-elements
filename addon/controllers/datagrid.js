import Controller from '@ember/controller';
import { action } from '@ember/object';
import { or } from '@ember/object/computed';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';
import {
    DEFAULT_DATAGRID_LIMIT, DEFAULT_DATAGRID_OFFSET, DEFAULT_DATAGRID_SORT_COLUMN
} from "../modules/datagrid/constants";
import DatagridQueryParams from '../modules/datagrid/datagrid-query-params';
import { TYPEAHEAD_WAIT_MILLISECONDS } from '../modules/datetime';

export default class DatagridController extends Controller {

    defaultQueryParamValues = new DatagridQueryParams();

    @service errorHandler;
    @service router;

    @tracked offset = DEFAULT_DATAGRID_OFFSET;
    @tracked limit = DEFAULT_DATAGRID_LIMIT;
    @tracked sort = DEFAULT_DATAGRID_SORT_COLUMN;
    @tracked fromDate = null;
    @tracked toDate = null;
    @tracked customDate = null;
    @tracked searchAttributes = null;
    @tracked searchText = null;
    @tracked searchWithinDateRange = false;

    sortByDefault = DEFAULT_DATAGRID_SORT_COLUMN;

    @or('model.{data,previousData}.value') datagridData;

    get datagridLoading() {
        return this.model.data.isRunning;
    }

    // Get comma-delimited attributes that should be searched.
    get datagridColumnsWithFilter() {
        return this.datagridColumns.filter(column => column.filter).map(column => column.name).join();
    }

    get filtered() {
        let filtered = this.offset !== DEFAULT_DATAGRID_OFFSET || this.limit !== DEFAULT_DATAGRID_LIMIT
            || this.sort !== this.sortByDefault
            || this.customDateSet
            || (this.searchText && this.searchText.length > 0);

        if(this.filters && !filtered) {
            filtered = Object.values(this.filters).some(filterObject => filterObject.properties.some(property => filterObject[property] !== false));
        }

        return filtered;
    }

    get customDateSet() {
        return this.fromDate !== this.defaultQueryParamValues.fromDate
            || this.toDate !== this.defaultQueryParamValues.toDate;
    }

    /*
        *** Example of date options getters.
    get fromDateOptions() {
        return {
            format: DEFAULT_FLATPICKR_DATE_FORMAT,
            minDate: moment().subtract(10, 'years').format(DEFAULT_DATE_FORMAT),
            maxDate: moment(this.toDate, DEFAULT_DATE_FORMAT).format(DEFAULT_DATE_FORMAT),
        };
    }

    get toDateOptions() {
        return {
            format: DEFAULT_FLATPICKR_DATE_FORMAT,
            minDate: moment(this.fromDate, DEFAULT_DATE_FORMAT).format(DEFAULT_DATE_FORMAT),
            maxDate: moment().format(DEFAULT_DATE_FORMAT)
        };
    }

    */

    @task({
        restartable: true
    })
    *taskFilterByText(text) {
        try {
            yield timeout(TYPEAHEAD_WAIT_MILLISECONDS);

            // Reset pagination as the filter often results in only one page being displayed.
            this.offset = DEFAULT_DATAGRID_OFFSET;

            if(text.length > 0) {
                this.searchAttributes = this.datagridColumnsWithFilter;
                this.searchText = text;
            } else {
                this.searchAttributes = null;
                this.searchText = null;
            }
        } catch(e) {
            this.errorHandler.handleErrors(e);
        }
    }

    @action
    filterByText(text) {
        this.taskFilterByText.perform(text)
    }

    @action
    updateDatagridSearchBarConfig() {
        if(this.limit === 'all') {
            this.datagridSearchBarConfig.showPagination = false;
            this.datagridSearchBarConfig.showRowInfo = true;
        } else {
            this.datagridSearchBarConfig.showPagination = true;
            this.datagridSearchBarConfig.showRowInfo = false;
        }
    }

    @action
    updateQueryParams(queryParams) {
        Object.keys(queryParams).forEach(param => this[param] = queryParams[param]);
    }

    @action
    resetFilters() {
        this.offset = DEFAULT_DATAGRID_OFFSET;
        this.limit = DEFAULT_DATAGRID_LIMIT;
        this.sort = DEFAULT_DATAGRID_SORT_COLUMN;
        this.searchAttributes = null;
        this.searchText = null;
        this.fromDate = null;
        this.toDate = null;
        this.customDate = null;
        this.searchWithinDateRange = false;
    }

    @action
    refresh() {
        this.send('routeRefresh');
    }

    @action
    changeFromDate(dates, dateString) {
        this.fromDate = dateString;
    }

    @action
    changeToDate(dates, dateString) {
        this.toDate = dateString;
    }

    @action
    updateFilter(filterObjectPath) {
        let filterObjectPathItems = filterObjectPath.split('.');
        let lastPathItemIndex = filterObjectPathItems.length - 1;
        let nestedFilterObject = filterObjectPathItems.reduce((filters, filterObjectPathItem, index, filterObjectPathItems) => (index === filterObjectPathItems.length - 2 ? filters[filterObjectPathItem] : filters), this.filters);
        nestedFilterObject[filterObjectPathItems[lastPathItemIndex]] = !nestedFilterObject[filterObjectPathItems[lastPathItemIndex]];
    }

    @action
    setCustomDate() {
        if(this.customDateSet) {
            this.offset = DEFAULT_DATAGRID_OFFSET;
            this.customDate = !this.customDate;
        }
    }

    /**
     * The following actions are listed as a guide to what other actions you may want for your grid.
     * They should all be located in your main datagrid controller and not here as they are context-sensitive.
     */
    /*@action
    rowClicked(rowData) {
        this.transitionToRoute('transactions.view', rowData.id);
    }

    // If there are custom filters then process them here.
    @action
    filterResults() {
        this.offset = '0';
    }*/
}
