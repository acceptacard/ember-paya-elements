import {tracked} from '@glimmer/tracking';
import {PERIOD_SINGLE} from '../../components/charity/cause-donation-form';

export default class CauseDonationForm {
    @tracked selectedCause = null;
    @tracked selectedCharity = null;
    @tracked reference = null;
    @tracked presetAmount = null;
    @tracked customAmount = null;
    @tracked period = PERIOD_SINGLE;
    @tracked customAmount1 = null;
    @tracked customAmount2 = null;
    @tracked customAmount3 = null;
    @tracked makeCustomAmountsDefault = false;
    @tracked hidePresetAmounts = false;
    @tracked hidePeriods = false;
    @tracked hideGiftAid = false;
    @tracked hideRegistration = false;
    @tracked fixedAmount = false;
    @tracked geolocation = false;
    @tracked allowEmail = false;

    get customAmounts() {
        return this.customAmount1 && this.customAmount2 && this.customAmount3 ? [this.customAmount1, this.customAmount2, this.customAmount3] : null;
    }
}
